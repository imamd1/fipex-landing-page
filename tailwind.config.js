/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['index.html'],
  theme: {
    container: {
      center: true,
      padding: '16px',
    },
    extend: {
      screen: {
        '2xl': '1320px',
      },
      colors: {
        primary: '#7DB2FE',
        secondary: '#EEF7FF',
        tertiary: '#2C3B8E'
      }
    },
  },
  plugins: [],
}
